# Issue Tracker
Read the README, dum dum

# IMPORTANT
For any exploits / hacks / bugs that should be considered private should be launched with staff via #tickets in dicsord in order to protect the publicity of the issue until fixed. Staff members will then escalate the issue to the development team.

## For Players
Welcome to the Vertue Roleplay issue tracker. This is where you'll be able to see & track reported issues and follow server development.

It is **RECOMMENDED** that you report any issues via #tickets in discord (https://discord.gg/bkXMukTaC8) as the staff members may have further insight to your bug.

## For Staff

Report bugs here:
https://gitlab.com/vertue/issue-tracker/-/issues/new?issuable_template=Bug%20Report

Add Feature Requests here:
https://gitlab.com/vertue/issue-tracker/-/issues/new?issuable_template=Feature%20Request

Report anything else for the development team here:
https://gitlab.com/vertue/issue-tracker/-/issues/new

## For Everyone
Please do not take any offence to development team's short replies. We're working as hard as we can to improve the server for everyone and do not have the time to format full support-like replies.

If you issue requires more information your issue will be closed, please re-open the issue with the extra information required. We're looking to avoid long-conversational reply threads.

Do **NOT** DM staff, developers, or management about your issue. We'll get around to it in due course and doing this will only slow us down.

## Issue Tags
https://gitlab.com/vertue/issue-tracker/-/labels
