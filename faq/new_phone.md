# Developer FAQ : vertue_phone

## Will I be able to search my contacts?
Yes.

## Will I be able to put/take out money into my business?
Yes.

## Will I be able to hire new employees?
Yes.

## Will I be able to delete my text messages?
Yes.

## Will I be able to clock in/out my state/business/other job using the phone?
Yes.

## Will I be able to mute notifications for certain apps?
Yes.

## Will I be able to answer the phone using the phone UI?
Yes.

## Will I be able to connect to w-fi spots?
Yes, this is how you'll be able to clock in for some businesses.

## Will I be able to block numbers? 
Yes, via your settings

## Will I be able to hide numbers?
No, there will be a "burner phone" (Old Nokia) that you can purchase to make these calls.

## Will I be able to use /911 or /311 via the phone?
No, for now that will stay as a command

## Will you be able to upload imgur links?
No, I am yet to think of a way to connect this while making it safe for younger players and Twitch streamers.

## Can we access PDM website via the phone?
No, as with imgur links the PDM website is outside of our control content-wise and could lead to issues with younger players / Twitch streamers. Need to think of how best to do it.

## Can we sort contacts?
No, this is a lot of work - they will go in alphabetic order by the first character.

## Can you make it so we can send selfies to eachother that are taken with the phone
Probably, but not on release.

## are text messages going to sort by most recent?
Yes

## will there be an unread message notification for new messages or missed calls?
Yes
