<!--- Verify first that your request is not already requested on GitLab -->
<!--- Complete *all* sections as described, this form is processed automatically -->

##### SUMMARY
<!--- Explain the request briefly below -->

##### ISSUE TYPE
- Feature Request

##### DATE / TIME
```paste below

```

##### REQUESTED BY 
<!--- Include player discord *REQUIRED*, chararacter ID *REQUIRED*, server ID -->
```paste below

```

##### AFFECTING 
<!--- Who does this affect? Everyone? One player? Cops? EMS? -->
```paste below

```


##### EXPECTED RESULTS
<!--- In depth details about the request -->
```paste below

```
<!--- HINT: You can paste gist.github.com links for larger files -->

##### Extra Information
<!--- Any extra information the players wants to add, including images -->
```paste below

```
