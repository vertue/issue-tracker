<!--- Verify first that your issue is not already reported on GitLab -->
<!--- Complete *all* sections as described, this form is processed automatically -->

##### SUMMARY
<!--- Explain the problem briefly below -->

##### ISSUE TYPE
- Bug Report

##### DATE / TIME
```paste below

```

##### REPORTED BY 
<!--- Include player discord *REQUIRED*, chararacter ID *REQUIRED*, server ID -->
```paste below

```

##### AFFECTING 
<!--- Who does this affect? Everyone? One player? Cops? EMS? -->
```paste below

```

##### STEPS TO REPRODUCE
<!--- Describe exactly how to reproduce the problem, using a minimal test-case -->
```paste below

```

<!--- HINT: You can paste gist.github.com links for larger files -->

##### EXPECTED RESULTS
<!--- Describe what player expected to happen when running the steps above -->
```paste below

```

##### ACTUAL RESULTS
<!--- Describe what actually happened. Be as verbose as possible -->
```paste below

```
